//
//  Entity.h
//  CultureMaze
//
//  Created by dotndot on 6/25/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Entity : NSManagedObject

@property (nonatomic) BOOL checked;
@property (nonatomic) int16_t ticketid;
@property (nonatomic, retain) NSString * name;
@property (nonatomic) NSTimeInterval relevantDate;
@property (nonatomic, retain) NSString * ticket;

@end
