//
//  ViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/17/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"

#import "SWRevealViewController.h"
@interface ViewController : UIViewController<NSURLSessionDelegate>
{
    SWRevealViewController *revealController;
    IBOutlet UITableView *eventsTableView;
    NSMutableArray *todayeventsData,*thisweekeventsData;
    int page;
    NSMutableDictionary *todaysaveStatus,*thisweeksaveStatus;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

