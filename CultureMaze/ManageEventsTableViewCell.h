//
//  ManageEventsTableViewCell.h
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageEventsTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *title,*time,*location,*timefontAwesome,*locationfontAwesome;
@property(nonatomic,strong)IBOutlet UIButton *culture,*category;
@end
