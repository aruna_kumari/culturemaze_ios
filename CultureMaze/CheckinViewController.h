//
//  CheckinViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/24/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import <sqlite3.h>
#import "Contact.h"
#import "AppDelegate.h"


@interface CheckinViewController : UIViewController<MGSwipeTableCellDelegate,NSURLSessionDelegate>
{
    NSArray *fetchedObjects;
    NSMutableDictionary *namearray,*checkinStatus;
    IBOutlet UITableView *eventTicketTableView;
    NSString        *databasePath;
    sqlite3 *timingsDatabase;
    NSArray *ticketHoldervalueKeys,*ticketIndex;
    Contact *contact;
    AppDelegate* dataappdelegate;

}


@end
