//
//  SwipeCheckTableViewCell.h
//  ANVI
//
//  Created by dotndot on 2/27/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
@interface SwipeCheckTableViewCell : MGSwipeTableCell
@property(nonatomic,strong)IBOutlet UIView *swipeView,*checkedview;
@property(nonatomic,strong)IBOutlet UILabel *name,*ticketType,*checked;
@property(nonatomic) BOOL checkin;
@property(nonatomic)int rowNumber;

@end
