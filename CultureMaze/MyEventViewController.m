//
//  MyEventViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "MyEventViewController.h"
#import "MyEventTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "Services.h"
@interface MyEventViewController ()

@end

@implementation MyEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
    }
   
    
    
    myEventData=[[NSMutableArray alloc]init];
    [self getMyEventsData:@"present"];
    // Do any additional setup after loading the view.
}
- (NSDate *)beginningOfDay
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                               fromDate:[NSDate date]];
    // NSLog(@"%@",[calendar dateFromComponents:components]);
    return [calendar dateFromComponents:components];
}
-(IBAction)tabAction:(UIButton *)sender
{
    myEventData=[[NSMutableArray alloc]init];
    if(sender.tag==0)
    {
        pastButton.backgroundColor=[UIColor lightGrayColor];
        liveButton.backgroundColor=[UIColor colorWithRed:0 green:122/255.0 blue:255/255.0 alpha:1.0];
        [liveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [pastButton setTitleColor:[UIColor colorWithRed:0 green:122/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];

        [self getMyEventsData:@"present"];
    }
    else
    {
        liveButton.backgroundColor=[UIColor lightGrayColor];
        pastButton.backgroundColor=[UIColor colorWithRed:0 green:122/255.0 blue:255/255.0 alpha:1.0];
        [pastButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [liveButton setTitleColor:[UIColor colorWithRed:0 green:122/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];

        [self getMyEventsData:@"past"];
    }
       
}
- (void)getMyEventsData:(NSString *)timeLine
{
    [SVProgressHUD show];

  //  NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    long startTime = (long)(NSTimeInterval)([[self beginningOfDay] timeIntervalSince1970]*1000);
   // [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"558a4b45d0711e4c19b06b7b" forKey:@"userId"];
    [passingArray setObject:@"LIVE" forKey:@"status"];
    [passingArray setObject:timeLine forKey:@"timeLine"];
    [passingArray setObject:[NSNumber numberWithInteger:startTime] forKey:@"millis"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:
                                                                        [NSString stringWithFormat:@"%@/v1/events/getUserCreatedEvents",serverURL]
                                                                        ]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              NSMutableArray *array=[res valueForKey:@"data"];
                                              for(int i=0;i<[array count];i++)
                                              {
                                                  [myEventData addObject:[array objectAtIndex:i]];
                                              }
                                              [SVProgressHUD dismiss];
                                              [myEventTableView reloadData];
                                              if([myEventData count]==0)
                                              {
                                                  [SVProgressHUD showErrorWithStatus:@"No Events are Available"];
                                              }
                                          }
                                          else
                                          {
                                              [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
                                          }
                                      }];
    [dataTask resume];
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    NSString *host = challenge.protectionSpace.host;
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        if ([host rangeOfString:@"qa.budagaa.com"].location != NSNotFound)
        {
            completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
        else
        {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge,nil);
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [myEventData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyEventTableViewCell *cell;
    //Get a cell instance (either dequeue from tableView or allocate a new one).
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MyEventTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];;
    }
    if([[[myEventData  objectAtIndex:indexPath.row] valueForKey:@"imgRef"]  isEqual:[NSNull null]] &&  [[[myEventData  objectAtIndex:indexPath.row] valueForKey:@"imgRef"]  length] == 0 )
    {
        if([[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"] count]!=0)
        {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"]objectAtIndex:0]]]];
        }
    }
    else
    {
        [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"imgRef"]]]];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // here set format of date which is in your output date (means above str with format)
    NSDate *date = [dateFormatter dateFromString: [[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"start"]valueForKey:@"date"]]; // here you can fetch date from string with define format
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy | HH:ss a"]; // here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date];
    cell.dateLabel.text=convertedString;
    cell.eventTitle.text=[NSString stringWithFormat:@"%@",[[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"title"]valueForKey:@"text"]];
    cell.manageEvent.tag=indexPath.row;
    [cell.manageEvent addTarget:self action:@selector(managEventScreen:) forControlEvents:UIControlEventTouchUpInside];
    NSString *address=nil;
    if([[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"])
    {
        address=[[[myEventData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"];
    }
    else{
        address=@"";
    }
    cell.cityLabel.text=[NSString stringWithFormat:@"%@",address];
    return cell;
}
-(void)managEventScreen:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setObject:[myEventData objectAtIndex:sender.tag]  forKey:@"ManageEvent"];
    [pref synchronize];
    [self performSegueWithIdentifier:@"ManageEvent" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
