//
//  Contact.h
//  JustForKids
//
//  Created by dotndot on 6/23/14.
//  Copyright (c) 2014 Shaurya Dosapati. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
@property (nonatomic, retain) NSString* name, *ticketType;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* phone,*status;
@property (nonatomic, retain) NSData* image;
-(id) initWithName:(NSString *)name ticketType:(NSString *)ticketType email:(NSString *)email status:(NSString *)status;

@end
