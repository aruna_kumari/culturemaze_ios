//
//  ViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/17/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "ViewController.h"
#import "HomeTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Services.h"
#import "CommonUtil.h"
#import "SVPullToRefresh.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Revealcontroller
  
    self.title=@"CultureMaze";
    todayeventsData=[[NSMutableArray alloc]init];
    thisweekeventsData=[[NSMutableArray alloc]init];

    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    __weak ViewController *weakSelf = self;
    [eventsTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertBottom];
    }];
    todaysaveStatus=[[NSMutableDictionary alloc]init];
    thisweeksaveStatus=[[NSMutableDictionary alloc]init];

//    [eventsTableView addPullToRefreshWithActionHandler:^{
//        [weakSelf getAccountInfoWithCompletion];
//    }];
    page=1;
    [SVProgressHUD show];
   
}
-(void)viewDidAppear:(BOOL)animated
{
      [self registerDevice];
}
-(void)insertBottom
{
        page++;
        int64_t delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [eventsTableView  beginUpdates];
            [self getTodayEventsData];
            [eventsTableView endUpdates];
            [eventsTableView.infiniteScrollingView stopAnimating];
        });
}
-(IBAction)menu:(UIBarButtonItem *)sender
{
    revealController =nil;
    revealController=[self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    [revealController revealToggle:sender];
}
-(void)registerDevice
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[user valueForKey:@"deviceID"] forKey:@"deviceId"];
    if([user valueForKey:@"DeviceToken"]!=nil)
        [passingArray setObject:[user valueForKey:@"DeviceToken"] forKey:@"token"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *URLPath = [NSString stringWithFormat:@"%@/v1/mobile/mregister",serverURL];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSMutableDictionary *dic=[user objectForKey:@"deviceInfo"];
    [request addValue:[NSString stringWithFormat:@"%@",[[dic valueForKey:@"Array"]valueForKey:@"Deviceid"]] forHTTPHeaderField:@"deviceID"];
    [request addValue:[NSString stringWithFormat:@"%@",[[dic valueForKey:@"Array"]valueForKey:@"DevicetimeZone"]] forHTTPHeaderField:@"DevicetimeZone"];
    [request addValue:[NSString stringWithFormat:@"%@",[[dic valueForKey:@"Array"]valueForKey:@"Deviceversion"]] forHTTPHeaderField:@"Deviceversion"];
    [request addValue:[NSString stringWithFormat:@"%@",[[dic valueForKey:@"Array"]valueForKey:@"Devicemodal"]] forHTTPHeaderField:@"Devicemodal"];
    [request addValue:[NSString stringWithFormat:@"%@",[[dic valueForKey:@"Array"]valueForKey:@"Devicename"]] forHTTPHeaderField:@"Devicename"];
    [request addValue:[NSString stringWithFormat:@"%@,%@",[user valueForKey:@"lat"],[user valueForKey:@"long"]] forHTTPHeaderField:@"DeviceLocation"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
   
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              if (res && [res isKindOfClass:[NSDictionary class]])
                                              {
                                                  NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                                  [pref setValue:[res valueForKey:@"id"] forKey:@"userid"];
                                                  [pref synchronize];
                                                  [self getTodayEventsData];
                                              }
                                          }
                                          else
                                          {
                                          }
                                      }];
    [dataTask resume];
}
- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:11];
    [dateComps setMinute:59];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}
-(NSDate *)dateAtEndingofDayForDate:(NSDate *)inputDate
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss a"];
    NSString *date=[dateFormat stringFromDate:inputDate];
    NSDate *dateRemaining = [dateFormat dateFromString:date];
    NSDate *pickerDate = dateRemaining;
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit
                                                             | NSMonthCalendarUnit
                                                             | NSDayCalendarUnit)
                                                   fromDate:pickerDate];
    NSDateComponents *timeComponents = [calendar components:(NSHourCalendarUnit
                                                             | NSMinuteCalendarUnit
                                                             | NSSecondCalendarUnit)
                                                   fromDate:pickerDate];
    // Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    [dateComps setHour:[timeComponents hour]];
    [dateComps setMinute:[timeComponents minute]];
    [dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
  //  NSLog(@"itemDate: %@", itemDate);
    return itemDate;
}
- (NSDate *)beginningOfDay
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit
                                               fromDate:[NSDate date]];
   // NSLog(@"%@",[calendar dateFromComponents:components]);
    return [calendar dateFromComponents:components];
}
- (NSDate *)endOfDay
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents *components = [NSDateComponents new];
    components.day = 1;
    NSDate *date = [calendar dateByAddingComponents:components
                                             toDate:self.beginningOfDay
                                            options:0];
    date = [date dateByAddingTimeInterval:-1];
    return date;
}
-(NSDate *)beginningOfweek:(NSDate *)date
{
    NSDate *today = [NSDate date];
    NSLog(@"Today date is %@",today);
     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];// you can use your format.
    
    //Week Start Date
    NSCalendar *gregorian = [[NSCalendar alloc]        initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorian components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:today];
    
    int dayofweek =(int) [[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:today] weekday];// this will give you current day of week
    
    [components setDay:([components day] - ((dayofweek) - 2))];// for beginning of the week.
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateFormat_first = [[NSDateFormatter alloc] init];
    [dateFormat_first setDateFormat:@"yyyy-MM-dd"];
   NSString  *dateString2Prev = [dateFormat stringFromDate:beginningOfWeek];
    
    NSDate  *weekstartPrev = [dateFormat_first dateFromString:dateString2Prev] ;
    
   // NSLog(@"%@",weekstartPrev);
    
    return weekstartPrev;
}

-(NSDate *)endOfWeek:(NSDate *)date
{
    NSDate *today = [NSDate date];
    NSLog(@"Today date is %@",today);
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    //Week End Date
    
    NSCalendar *gregorianEnd = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *componentsEnd = [gregorianEnd components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:today];
    
    int Enddayofweek = (int)[[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:today] weekday];// this will give you current day of week
    
    [componentsEnd setDay:([componentsEnd day]+(7-Enddayofweek)+1)];// for end day of the week
    
    NSDate *EndOfWeek = [gregorianEnd dateFromComponents:componentsEnd];
    NSDateFormatter *dateFormat_End = [[NSDateFormatter alloc] init];
    [dateFormat_End setDateFormat:@"yyyy-MM-dd"];
    NSString  * dateEndPrev = [dateFormat stringFromDate:EndOfWeek];
    
    NSDate *weekEndPrev = [dateFormat_End dateFromString:dateEndPrev] ;
    //NSLog(@"%@",weekEndPrev);
    return weekEndPrev;
}
- (void)getTodayEventsData
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    long startTime = (long)(NSTimeInterval)([[self beginningOfDay] timeIntervalSince1970]*1000);
    long endTime = (long)(NSTimeInterval)([[self endOfDay] timeIntervalSince1970]*1000);

    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:
                              [NSString stringWithFormat:@"%@/v1/events/search/%@/-/-/-/-/-/-/-/%d/-/-/_id/-/%ld/%ld",serverURL,[user valueForKey:@"userid"],page,startTime,endTime]
                              ]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              NSMutableArray *array=[res valueForKey:@"data"];
                                              for(int i=0;i<[array count];i++)
                                              {
                                                  [todayeventsData addObject:[array objectAtIndex:i]];
                                              }
                                              [self getThisWeekEventsData];
                                          }
                                          else
                                          {
                                              [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
                                          }
                                          [SVProgressHUD dismiss];
                                      }];
    [dataTask resume];
}
- (void)getThisWeekEventsData
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    long startTime = (long)(NSTimeInterval)([[self beginningOfweek:[NSDate date]] timeIntervalSince1970]*1000);
    long endTime = (long)(NSTimeInterval)([[self endOfWeek:[NSDate date]] timeIntervalSince1970]*1000);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:
                              [NSString stringWithFormat:@"%@/v1/events/search/%@/-/-/-/-/-/-/-/%d/-/-/_id/-/%ld/%ld",serverURL,[user valueForKey:@"userid"],page,startTime,endTime]
                              ]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              NSMutableArray *array=[res valueForKey:@"data"];
                                              if([array count]!=0)
                                              {
                                              for(int i=0;i<[array count];i++)
                                              {
                                                  [thisweekeventsData addObject:[array objectAtIndex:i]];
                                                  
                                              }
                                              [eventsTableView reloadData];
                                              [SVProgressHUD dismiss];
                                              }
                                              if([thisweekeventsData count]==0)
                                              {
                                              [SVProgressHUD showInfoWithStatus:@"No Events are Available"];
                                              }
                                          }
                                          else
                                          {
                                              [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
                                          }
                                      }];
    [dataTask resume];
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    NSString *host = challenge.protectionSpace.host;
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        if ([host rangeOfString:@"qa.budagaa.com"].location != NSNotFound)
        {
            completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
        else
        {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge,nil);
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if([todayeventsData count]!=0 && [thisweekeventsData count]!=0)
    return 2;
    else if([todayeventsData count]!=0 || [thisweekeventsData count]!=0)
    return 2;
    else
    return 0;
        
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section==0)
    return [todayeventsData  count];
    else
    return [thisweekeventsData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeTableViewCell *cell;
    //Get a cell instance (either dequeue from tableView or allocate a new one).
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];;
    }
    if(indexPath.section==0)
    {
        if([todayeventsData count]!=0)
        {
        if([[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"bannerImgRefs"] count]==0)
        {
            if([[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"] count]!=0)
            {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"]objectAtIndex:0]]]];
            }
        }
        else
        {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"bannerImgRefs"]objectAtIndex:0]]]];
        }
    [cell.saveButton addTarget:self action:@selector(todaySaveEvent:) forControlEvents:UIControlEventTouchUpInside];
    cell.saveButton.tag=indexPath.row;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // here set format of date which is in your output date (means above str with format)
    NSDate *date = [dateFormatter dateFromString: [[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"start"]valueForKey:@"date"]]; // here you can fetch date from string with define format
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy | HH:ss a"]; // here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date];
    cell.dateLabel.text=convertedString;
    cell.eventTitle.text=[NSString stringWithFormat:@"%@",[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"title"]valueForKey:@"text"]];
    [cell.categoryLabel setTitle:[NSString stringWithFormat:@"#%@",[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"category"]objectAtIndex:0]] forState:UIControlStateNormal];
    
    [cell.cultureLabel setTitle:[NSString stringWithFormat:@"#%@",[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"culture"]objectAtIndex:0]] forState:UIControlStateNormal];
    
    NSString *address=nil;
    if([[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"])
    {
        address=[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"state"];
    }
    else{
        address=@"";
    }
//    if([[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"city"])
//    {
//        if([address length]!=0)
//            address=[NSString stringWithFormat:@"%@,%@",address,[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"city"]];
//        else
//            address=[NSString stringWithFormat:@"%@",[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"city"]];
//    }
    cell.cityLabel.text=[NSString stringWithFormat:@"%@",address];

    
    cell.likeCount.text=[NSString stringWithFormat:@"%d",[[[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"counts"]valueForKey:@"views"]intValue]];
    cell.saveLabel.font = [UIFont fontWithName:@"fontawesome" size:28.0f];
    
    [cell.cultureLabel addTarget:self action:@selector(todaycultureData:) forControlEvents:UIControlEventTouchUpInside];
    cell.cultureLabel.tag=indexPath.row;
    cell.categoryLabel.tag=indexPath.row;
    [cell.categoryLabel addTarget:self action:@selector(todaycategoryData:) forControlEvents:UIControlEventTouchUpInside];
            if([todayeventsData valueForKey:[NSString stringWithFormat:@"%ld",(long)cell.saveButton.tag]])
            {
                cell.saveLabel.text=@"\uf004";
                cell.saveLabel.textColor=[UIColor blueColor];
                [cell.saveButton addTarget:self action:@selector(todayDeleteEvent:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                cell.saveLabel.text =@"\uf08a";
                cell.saveLabel.textColor=[UIColor whiteColor];
                [cell.saveButton addTarget:self action:@selector(todaySaveEvent:) forControlEvents:UIControlEventTouchUpInside];
            }
            if(!cell.saveORdelete)
            {
                NSMutableArray *array=[[todayeventsData  objectAtIndex:indexPath.row]valueForKey:@"actionTypes"];
                if([array containsObject:@"save"])
                {
                    cell.saveLabel.text=@"\uf004";
                    cell.saveLabel.textColor=[UIColor blueColor];
                    [cell.saveButton addTarget:self action:@selector(todayDeleteEvent:) forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    [cell.saveButton addTarget:self action:@selector(todaySaveEvent:) forControlEvents:UIControlEventTouchUpInside];
                }
                
            }
    }
    }
    else
    {
        if([[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"bannerImgRefs"] count]==0)
        {
             if([[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"] count]!=0)
             {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"]objectAtIndex:0]]]];
             }
        }
        else
        {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"bannerImgRefs"]objectAtIndex:0]]]];
        }
        cell.saveButton.tag=indexPath.row;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // here set format of date which is in your output date (means above str with format)
        NSDate *date = [dateFormatter dateFromString: [[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"start"]valueForKey:@"date"]]; // here you can fetch date from string with define format
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy | HH:ss a"]; // here set format which you want...
        NSString *convertedString = [dateFormatter stringFromDate:date];
        cell.dateLabel.text=convertedString;
        cell.eventTitle.text=[NSString stringWithFormat:@"%@",[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"title"]valueForKey:@"text"]];
        [cell.categoryLabel setTitle:[NSString stringWithFormat:@"#%@",[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"category"]objectAtIndex:0]] forState:UIControlStateNormal];
        
        [cell.cultureLabel setTitle:[NSString stringWithFormat:@"#%@",[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"culture"]objectAtIndex:0]] forState:UIControlStateNormal];
        
        NSString *address=nil;
        if([[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"])
        {
            address=[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"];
        }
        else
        {
            address=@"";
        }
        cell.cityLabel.text=[NSString stringWithFormat:@"%@",address];
        cell.likeCount.text=[NSString stringWithFormat:@"%d",[[[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"counts"]valueForKey:@"views"]intValue]];
        cell.saveLabel.font = [UIFont fontWithName:@"fontawesome" size:28.0f];
        
        [cell.cultureLabel addTarget:self action:@selector(thisweekcultureData:) forControlEvents:UIControlEventTouchUpInside];
        cell.cultureLabel.tag=indexPath.row;
        cell.categoryLabel.tag=indexPath.row;
        [cell.categoryLabel addTarget:self action:@selector(thisweekcategoryData:) forControlEvents:UIControlEventTouchUpInside];
        if([thisweeksaveStatus valueForKey:[NSString stringWithFormat:@"%ld",(long)cell.saveButton.tag]])
        {
            cell.saveLabel.text=@"\uf004";
            cell.saveLabel.textColor=[UIColor blueColor];
            [cell.saveButton addTarget:self action:@selector(thisWeekDeleteEvent:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.saveLabel.text =@"\uf08a";
            cell.saveLabel.textColor=[UIColor whiteColor];
            [cell.saveButton addTarget:self action:@selector(thisWeekSaveEvent:) forControlEvents:UIControlEventTouchUpInside];
        }
        if(!cell.saveORdelete)
        {
            NSMutableArray *array=[[thisweekeventsData  objectAtIndex:indexPath.row]valueForKey:@"actionTypes"];
            if([array containsObject:@"save"])
            {
                cell.saveLabel.text=@"\uf004";
                cell.saveLabel.textColor=[UIColor blueColor];
                [cell.saveButton addTarget:self action:@selector(thisWeekDeleteEvent:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                [cell.saveButton addTarget:self action:@selector(thisWeekSaveEvent:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
    }
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor=[UIColor colorWithRed:241.0/255.0f green:245/255.0f blue:247/255.0 alpha:1.0];
    UILabel *headerTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, self.view.frame.size.width, 42)];
    headerTitle.backgroundColor=[UIColor whiteColor];
    headerTitle.font=[UIFont systemFontOfSize:14.00];
    if(section==0)
    {
       headerTitle.text=@"  TODAY'S EVENTS";
    }
    else
    {
        headerTitle.text=@"  THIS WEEK EVENTS";
  
    }
    headerTitle.textColor=[UIColor darkGrayColor];
    [view addSubview:headerTitle];
    return view;
}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        if([todayeventsData count]!=0)
        return 52;
        else
        return 1;
    }
    else
    {
        return 52;
    }
}
-(void)thisweekcultureData:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[thisweekeventsData  objectAtIndex:sender.tag]valueForKey:@"culture"]objectAtIndex:0]] forKey:@"cName"];
    [pref setValue:@"culture" forKey:@"actionType"];
    [pref synchronize];
    [self performSegueWithIdentifier:@"Culture" sender:nil];
}

-(void)thisweekcategoryData:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[thisweekeventsData  objectAtIndex:sender.tag]valueForKey:@"category"]objectAtIndex:0]]  forKey:@"cName"];
    [pref setValue:@"category" forKey:@"actionType"];
    [pref synchronize];
    [self performSegueWithIdentifier:@"Culture" sender:nil];
}

-(void)todaycultureData:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[todayeventsData  objectAtIndex:sender.tag]valueForKey:@"culture"]objectAtIndex:0]] forKey:@"cName"];
    [pref setValue:@"culture" forKey:@"actionType"];
    [pref synchronize];
    [SVProgressHUD dismiss];
    [self performSegueWithIdentifier:@"Culture" sender:nil];
}
-(void)todaycategoryData:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[todayeventsData  objectAtIndex:sender.tag]valueForKey:@"category"]objectAtIndex:0]]  forKey:@"cName"];
     [pref setValue:@"category" forKey:@"actionType"];
    [pref synchronize];
    [SVProgressHUD dismiss];

    [self performSegueWithIdentifier:@"Culture" sender:nil];
}
-(void)todaySaveEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[eventsTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf004";
    cell.saveLabel.textColor=[UIColor blueColor];
    cell.saveORdelete=YES;
    [todaysaveStatus setValue:[NSString stringWithFormat:@"%ld", (long)sender.tag] forKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[todayeventsData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"A" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self saveEvent:jsonString];
}
-(void)todayDeleteEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[eventsTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf08a";
    cell.saveORdelete=YES;
    cell.saveLabel.textColor=[UIColor whiteColor];
    
    [todaysaveStatus removeObjectForKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[todayeventsData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"D" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self saveEvent:jsonString];
}
-(void)thisWeekDeleteEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:1] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[eventsTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf08a";
    cell.saveORdelete=YES;
    cell.saveLabel.textColor=[UIColor whiteColor];
    
    [thisweeksaveStatus removeObjectForKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[thisweekeventsData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"D" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self saveEvent:jsonString];
}
-(void)thisWeekSaveEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:1] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[eventsTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf004";
    cell.saveORdelete=YES;
    cell.saveLabel.textColor=[UIColor blueColor];

    [thisweeksaveStatus setValue:[NSString stringWithFormat:@"%ld", (long)sender.tag] forKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[thisweekeventsData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"A" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self saveEvent:jsonString];
}
-(void)saveEvent:(NSString *)jsonString
{
    NSString *URLPath = [NSString stringWithFormat:@"%@/v1/events/updateCounts",serverURL];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        if (!error && responseCode == 200)
        {
            id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (res && [res isKindOfClass:[NSDictionary class]])
            {
                
            }
        }
        else
        {
            if([[error domain] isEqualToString:@"NSURLErrorDomain"])
            {
                
            }
        }
        [eventsTableView reloadData];

    }];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    [searchBar becomeFirstResponder];
}
//search cancel button event
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}
//search enter characters or remove characters(backspace)
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}
-(void)viewWillDisappear:(BOOL)animated
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
