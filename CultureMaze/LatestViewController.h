//
//  LatestViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/22/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <sqlite3.h>
@interface LatestViewController : UIViewController<NSURLSessionDelegate>
{
    IBOutlet UITableView *latestEventsTableView;
    NSMutableArray *latestEventData;
    NSMutableDictionary *latestStatusData;
    int page;
    NSString        *databasePath;
    sqlite3 *timingsDatabase;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
