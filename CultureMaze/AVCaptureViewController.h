//
//  AVCaptureViewController.h
//  QRCodeReader
//
//  Created by dotndot on 2/25/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface AVCaptureViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate,NSURLSessionDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@end
