//
//  CheckinViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/24/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "CheckinViewController.h"
#import "SwipeCheckTableViewCell.h"
#import "SVProgressHUD.h"
#import "SWRevealViewController.h"
#import "Services.h"
#import "MGSwipeButton.h"

@interface CheckinViewController ()

@end

@implementation CheckinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataappdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    namearray=[[NSMutableDictionary alloc]init];
    checkinStatus=[[NSMutableDictionary alloc]init];
    
    // Do any additional setup after loading the view.
    ticketIndex = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    databasePath = [[NSString alloc]initWithString:[documentsDirectory stringByAppendingPathComponent:@"events.db"]];
    [SVProgressHUD show];
}
-(void)viewWillAppear:(BOOL)animated
{
    namearray=[[NSMutableDictionary alloc]init];
    checkinStatus=[[NSMutableDictionary alloc]init];
    [self onlinedata];
}
-(void)onlinedata
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableArray *eventArray=[user objectForKey:@"ManageEvent"];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[eventArray valueForKey:@"_id"] forKey:@"eventId"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *string=[NSString stringWithFormat:@"%@/v1/events/checkIn",serverURL];
    NSURL *URL = [NSURL URLWithString:[string stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              fetchedObjects=[res valueForKey:@"tickets"];
                                             // [dataappdelegate getOfflineData:fetchedObjects];
                                              //[dataappdelegate getTimings];
                                             // fetchedObjects=[dataappdelegate findByRegisterNumber];
                                              for(int i=0;i<[fetchedObjects count];i++)
                                              {
                                                  contact =[[Contact alloc]init];
                                                  NSString *name=[[[[res valueForKey:@"tickets"]valueForKey:@"userInfo"] valueForKey:@"displayName"] objectAtIndex:i];
                                                  NSString * firstLetter = [name substringWithRange:[name rangeOfComposedCharacterSequenceAtIndex:0]];
                                                  if(![namearray objectForKey:firstLetter])
                                                  {
                                                      if([[[fetchedObjects valueForKey:@"status"]objectAtIndex:i]  isEqual:[NSNull null]])
                                                      {
                                                      [namearray setObject:[NSArray arrayWithObject:[contact initWithName:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"displayName"] objectAtIndex:i] ticketType:[NSString stringWithFormat:@"%@ (number of tickets %ld)",[[fetchedObjects valueForKey:@"type"]objectAtIndex:i],(long)[[[fetchedObjects valueForKey:@"totalCount"]objectAtIndex:i] integerValue]] email:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"email"] objectAtIndex:i] status:@""]]  forKey:firstLetter];
                                                      }
                                                      else
                                                      {
                                                     [namearray setObject:[NSArray arrayWithObject:[contact initWithName:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"displayName"] objectAtIndex:i] ticketType:[NSString stringWithFormat:@"%@ (number of tickets %ld)",[[fetchedObjects valueForKey:@"type"]objectAtIndex:i],(long)[[[fetchedObjects valueForKey:@"totalCount"]objectAtIndex:i] integerValue]] email:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"email"] objectAtIndex:i] status:[[fetchedObjects valueForKey:@"status"]objectAtIndex:i]]]   forKey:firstLetter];
                                                      }
                                                  }
                                                  else
                                                  {
                                                      NSMutableDictionary *statsCopy = [namearray objectForKey:firstLetter];
                                                      for(int j=0;j<[statsCopy count];j++)
                                                      {
                                                          if([[[fetchedObjects valueForKey:@"status"]objectAtIndex:i ]  isEqual:[NSNull null]])
                                                          {
                                                          [namearray setObject:[NSArray arrayWithObjects:[contact initWithName:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"displayName"] objectAtIndex:i] ticketType:[NSString stringWithFormat:@"%@ (number of tickets %ld)",[[fetchedObjects valueForKey:@"type"]objectAtIndex:i],(long)[[[fetchedObjects valueForKey:@"totalCount"]objectAtIndex:i] integerValue]] email:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"email"] objectAtIndex:i] status:@""] ,[[namearray objectForKey:firstLetter] objectAtIndex:j],nil] forKey:firstLetter];
                                                          }
                                                          else
                                                          {
                                                              [namearray setObject:[NSArray arrayWithObjects:[contact initWithName:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"displayName"] objectAtIndex:i] ticketType:[NSString stringWithFormat:@"%@ (number of tickets %ld)",[[fetchedObjects valueForKey:@"type"]objectAtIndex:i],(long)[[[fetchedObjects valueForKey:@"totalCount"]objectAtIndex:i] integerValue]] email:[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"email"] objectAtIndex:i] status:[[fetchedObjects valueForKey:@"status"]objectAtIndex:i]] ,[[namearray objectForKey:firstLetter] objectAtIndex:j],nil] forKey:firstLetter];
                                                          }
                                                      }
                                                  }
                                              }
                                              ticketHoldervalueKeys = [[namearray allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                                              if([fetchedObjects count]==0)
                                              {
                                                  [SVProgressHUD showInfoWithStatus:@"No Tickets Available"];
                                              }
                                              [eventTicketTableView reloadData];
                                          }
                                          else
                                          {
                                          }
                                          [SVProgressHUD dismiss];
                                      }];
    [dataTask resume];
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    NSString *host = challenge.protectionSpace.host;
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        if ([host rangeOfString:@"qa.budagaa.com"].location != NSNotFound)
        {
            completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
        else
        {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge,nil);
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return ticketIndex;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [ticketHoldervalueKeys count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [ticketHoldervalueKeys  objectAtIndex:section];
    NSArray *sectionAnimals = [namearray objectForKey:sectionTitle];
    NSLog(@"%@",sectionAnimals);
    return [sectionAnimals count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [ticketHoldervalueKeys objectAtIndex:section];
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [ticketHoldervalueKeys indexOfObject:title];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SwipeCheckTableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"SwipeCheckTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];;
    }
    cell.tag=indexPath.section;
    NSString *sectionTitle = [ticketHoldervalueKeys objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [namearray objectForKey:sectionTitle];
    contact=[sectionAnimals objectAtIndex:indexPath.row];
   
    if([contact.status isEqualToString:@"A"])
    {
        cell.checkin=YES;
    }
    else
    {
        cell.checkin=NO;
    }
    if(cell.checkin)
    {
        CGRect frame=cell.swipeView.frame;
        frame.origin.x=20;
        cell.swipeView.frame=frame;
        cell.delegate = self;
        cell.checkedview.alpha=1.00;
    }
    else
    {
        CGRect frame=cell.swipeView.frame;
        frame.origin.x=0;
        cell.swipeView.frame=frame;
        cell.delegate = self;
        cell.checkedview.alpha=0.00;
    }
  
    cell.name.text=contact.name;
    cell.ticketType.text=contact.ticketType;
    cell.rowNumber=(int)indexPath.row;
    cell.checked.text=contact.email;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(BOOL)swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    swipeSettings.transition = MGSwipeTransitionBorder;
    expansionSettings.buttonIndex = 0;
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.fillOnTrigger = NO;
        expansionSettings.threshold = 2;
        return @[[MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"check"] backgroundColor:[UIColor colorWithRed:122/255.0 green:209/255.0 blue:8/255.0f alpha:1.0] padding:40 callback:^BOOL(MGSwipeTableCell *sender)
                  {
                      [self updateCellIndicactor:(SwipeCheckTableViewCell *)sender];
                      [(UIButton*)[cell.leftButtons objectAtIndex:0] setTitle:nil forState:UIControlStateNormal];
                      [(UIButton*)[cell.leftButtons objectAtIndex:0] addTarget:self action:@selector(CheckMark:) forControlEvents:UIControlEventTouchUpInside];
                      return YES;
                  }]];
    }
    return nil;
}
-(void)updateCellIndicactor:(SwipeCheckTableViewCell *) cell
{
    if(![self conncetion])
    {
        [dataappdelegate Update:[[fetchedObjects valueForKey:@"_id"] objectAtIndex:cell.tag]];
    }
    else
    {
        NSString *sectionTitle = [ticketHoldervalueKeys objectAtIndex:cell.tag];
        NSArray *sectionAnimals = [namearray objectForKey:sectionTitle];
        contact=[sectionAnimals objectAtIndex:cell.rowNumber];
        NSLog(@"%@",contact.name);
        //upload data to server
        NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
        if(!cell.checkin)
        {
            CGRect frame=cell.swipeView.frame;
            frame.origin.x=20;
            cell.swipeView.frame=frame;
            cell.checkedview.alpha=1.00;
            [checkinStatus setValue:[NSString stringWithFormat:@"%ld", (long)cell.tag] forKey:[NSString stringWithFormat:@"%ld",(long)cell.tag]];
            cell.checkin=YES;
            [eventTicketTableView reloadData];
            [passingArray setObject:@"A" forKey:@"status"];
            contact.status=@"A";
        }
        else
        {
            CGRect frame=cell.swipeView.frame;
            frame.origin.x=0;
            cell.swipeView.frame=frame;
            cell.checkedview.alpha=0.00;
            [checkinStatus removeObjectForKey:[NSString stringWithFormat:@"%ld",(long)cell.tag]];
            cell.checkin=NO;
            [eventTicketTableView reloadData];
            [passingArray setObject:@"NA" forKey:@"status"];
            contact.status=@"";

        }
        [passingArray setObject:[[fetchedObjects valueForKey:@"_id"] objectAtIndex:cell.tag] forKey:@"ticketId"];
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSString *string=[NSString stringWithFormat:@"%@/v1/tickets/updateCount",serverURL];
        NSURL *URL = [NSURL URLWithString:[string stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                              if (error == nil && data != nil)
                                              {
                                                  id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              }
                                              else
                                              {
                                              }
                                              [SVProgressHUD dismiss];
                                          }];
        [dataTask resume];
    }
}
-(BOOL)conncetion
{
    NSString *URLString = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"] encoding:NSUTF8StringEncoding error:nil];
    return ( URLString != NULL ) ? YES : NO;
}
-(void)CheckMark:(UIButton *)sender
{
    NSLog(@"check mark clicked");
}
-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state)
    {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
    NSLog(@"Swipe state: %@ ::: Gesture: %@", str, gestureIsActive ? @"Active" : @"Ended");
}
-(IBAction)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
