//
//  MyEventTableViewCell.h
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyEventTableViewCell : UITableViewCell
{
IBOutlet UIView *eventView;
}
@property(nonatomic,strong)    IBOutlet UIImageView *eventImage;
@property(nonatomic,strong)    IBOutlet UILabel *eventTitle,*dateLabel,*username,*likeCount,*saveLabel,*cityLabel,*timeFontawesome,*loationFontasesome;
@property (nonatomic,strong)IBOutlet UIButton *manageEvent;
@property(nonatomic)BOOL saveORdelete;
@end
