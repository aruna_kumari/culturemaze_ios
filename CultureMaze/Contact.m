//
//  Contact.m
//  JustForKids
//
//  Created by dotndot on 6/23/14.
//  Copyright (c) 2014 Shaurya Dosapati. All rights reserved.
//

#import "Contact.h"

@implementation Contact
-(id) initWithName:(NSString *)name ticketType:(NSString *)ticketType email:(NSString *)email status:(NSString *)status
{
    self = [super init];
    if(self)
    {
        self.name = name;
        self.ticketType=ticketType;
        self.email=email;
        self.status=status;
    }
    return self;
}

@end
