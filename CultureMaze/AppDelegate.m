//
//  AppDelegate.m
//  CultureMaze
//
//  Created by dotndot on 6/17/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonUtil.h"
#import "SSKeychain.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize locationManager;
-(void)createEditableCopyOfDatabaseIfNeeded
{
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        
        NSString *charsTableNameQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS Events(ID INTEGER PRIMARY KEY, name TEXT, ticket TEXT,relevantDate TEXT,checked BOOL)"];
        
        int results = 0;
        
        //create all chars tables
        const char *charsTableNameQuerySQL = [charsTableNameQuery UTF8String];
        sqlite3_stmt * charsTableNameStatment = nil;
        results = sqlite3_exec(database, charsTableNameQuerySQL, NULL, NULL, NULL);
        if (results != SQLITE_DONE) {
            const char *err = sqlite3_errmsg(database);
            NSString *errMsg = [NSString stringWithFormat:@"%s",err];
            if (![errMsg isEqualToString:@"not an error"]) {
                NSLog(@"createTables-chartables error: %@",errMsg);
            }
        }
        
        sqlite3_finalize(charsTableNameStatment);
        sqlite3_close(database);
    }
    else{
        NSLog(@"database not opened");
    }
}

-(void)initializeDataStructures
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Events.sqlite"];
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK)
    {
        printf("Database successfully opened");
    }
    else
    {
        sqlite3_close(database);
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(database));
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Notification
    [self createEditableCopyOfDatabaseIfNeeded];
    [self initializeDataStructures];
    if([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            [application registerForRemoteNotifications]; // <--- CRASH HERE
        }
        else
        {
            [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
        }
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            [application registerForRemoteNotifications]; // <--- CRASH HERE
        }
        else
        {
            [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
        }
    }
    
    //startupdating location information
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = 500;
    if([[UIDevice currentDevice].systemVersion floatValue]>=8.0)
    {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestAlwaysAuthorization];
            [locationManager requestWhenInUseAuthorization];
        }
    }
    if([self shouldFetchUserLocation])
    [locationManager startUpdatingLocation];
    [self deviceInfo];
    return YES;
}
-(NSString *)getUniqueDeviceIdentifierAsString
{
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"CMMetadata"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:@"CMMetadata"];
    }
    return strApplicationUUID;
}
-(void)deviceInfo
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    UIDevice *aDevice = [UIDevice currentDevice];
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[self getUniqueDeviceIdentifierAsString] forKey:@"Deviceid"];
    [passingArray setObject:[aDevice name] forKey:@"Devicename"];
    [passingArray setObject:[aDevice model] forKey:@"Devicemodal"];
    [passingArray setObject:[aDevice systemVersion] forKey:@"Deviceversion"];
    [passingArray setObject:[user objectForKey:@"AppleLocale"]!=nil ?[user objectForKey:@"AppleLocale"]:@"" forKey:@"DeviceLocal"];
    NSString *timeZoneString = [timeZone localizedName:NSTimeZoneNameStyleShortStandard locale:currentLocale];
    if([timeZone isDaylightSavingTimeForDate:[NSDate date]])
    {
        timeZoneString = [timeZone localizedName:NSTimeZoneNameStyleShortDaylightSaving locale:currentLocale];
    }
    [passingArray setObject:timeZoneString forKey:@"DevicetimeZone"];
    NSArray *subviews = [[[[UIApplication sharedApplication] valueForKey:@"statusBar"] valueForKey:@"foregroundView"]subviews];
    NSNumber *dataNetworkItemView = nil;
    for (id subview in subviews)
    {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]])
        {
            dataNetworkItemView = subview;
            break;
        }
    }
    switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"]integerValue]) {
        case 0:
            ////NSLog(@"No wifi or cellular");
            [passingArray setObject:@"No wifi or cellular" forKey:@"DeviceNet"];
            break;
        case 1:
            ////NSLog(@"2G");
            [passingArray setObject:@"2G" forKey:@"DeviceNet"];
            break;
        case 2:
            ////NSLog(@"3G");
            [passingArray setObject:@"3G" forKey:@"DeviceNet"];
            break;
        case 3:
            ////NSLog(@"4G");
            [passingArray setObject:@"4G" forKey:@"DeviceNet"];
            break;
        case 4:
            ////NSLog(@"LTE");
            [passingArray setObject:@"LTE" forKey:@"DeviceNet"];
            break;
        case 5:
            ////NSLog(@"Wifi");
            [passingArray setObject:@"Wifi" forKey:@"DeviceNet"];
            break;
        default:
            break;
    }
    NSMutableArray *deviceinfo=[NSMutableArray new];
    [deviceinfo addObject:passingArray];
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:passingArray options:NSJSONWritingPrettyPrinted error:&error];
    
    NSArray *jsonArray = (NSArray *)[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    NSDictionary *dict = @{@"Array" : jsonArray};
    NSData *json;
    NSString *jsonString=nil;
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:dict])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        // If no errors, let's view the JSON
        if (json != nil && error == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        }
    }
    NSString *jsondataString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [user setValue:[self getUniqueDeviceIdentifierAsString] forKey:@"deviceID"];
    [user setObject:dict forKey:@"deviceInfo"];
    [user setValue:jsondataString forKey:@"deviceInfo1"];
    [user setValue:nil forKey:@"category"];
    [user setObject:@"Home" forKey:@"tabname"];
    [user setBool:NO forKey:@"willapear"];
    [user setBool:NO forKey:@"willapear1"];
    [user synchronize];
}
-(BOOL)shouldFetchUserLocation{
    
    BOOL shouldFetchLocation= NO;
    if ([CLLocationManager locationServicesEnabled])
    {
        switch ([CLLocationManager authorizationStatus])
    {
            case kCLAuthorizationStatusAuthorized:
                shouldFetchLocation= YES;
                break;
            case kCLAuthorizationStatusDenied:
            {
//                UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Error" message:@"App level settings has been denied" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
               // alert= nil;
            }
            break;
            case kCLAuthorizationStatusNotDetermined:
            {
//                UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Error" message:@"The user is yet to provide the permission" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//                alert= nil;
            }
            break;
            case kCLAuthorizationStatusRestricted:
            {
//                UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Error" message:@"The app is recstricted from using location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
//                alert= nil;
            }
            break;
            default:
                break;
        }
    }
    else{
//        UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Error" message:@"The location services seems to be disabled from the settings." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//        alert= nil;
    }
    
    return shouldFetchLocation;
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@", error.localizedDescription);
}
//Location Updation method
-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    //update user location in Preferences
    [CommonUtil updateUserLocation:newLocation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"checklocationupdate" object:nil userInfo:nil];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *token =[[deviceToken description] stringByTrimmingCharactersInSet:
                      [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
   // [PublicMethod register];
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"%@",error);
}
- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"%ld",(long)[UIApplication sharedApplication].applicationIconBadgeNumber);
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue]+[UIApplication sharedApplication].applicationIconBadgeNumber;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setObject:@"Notification" forKey:@"RemoteNotify"];
    [pref setObject:[[userInfo valueForKey:@"aps"] valueForKey:@"name"] forKey:@"notifyvideoid"];
    [pref synchronize];
}

-(BOOL)application:(UIApplication *)application
           openURL:(NSURL *)url
 sourceApplication:(NSString *)sourceApplication
        annotation:(id)annotation
{
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}
-(IBAction)getOfflineData:(NSArray *)fetchedObjects
{
    for(int i=0;i<[fetchedObjects count];i++)
    {
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            BOOL success = false;
            sqlite3_stmt *statement = NULL;
            const char *dbpath = [databasePath UTF8String];
            if (sqlite3_open(dbpath, &database) == SQLITE_OK)
            {
                    NSLog(@"New data, Insert Please");
                    NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO Events (ID, name,ticket,relevantDate,checked) VALUES (\"%ld\", \"%@\",\"%@\", \"%@\",\"%@\")", (long)[[[fetchedObjects valueForKey:@"_id"] objectAtIndex:i] integerValue], [[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"firstName"] objectAtIndex:i],[[fetchedObjects valueForKey:@"type"] objectAtIndex:i],[[fetchedObjects valueForKey:@"updateDt"] objectAtIndex:i],@"0"];
                    
                    const char *insert_stmt = [insertSQL UTF8String];
                    sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        success = true;
                        NSLog(@"insertion Success");
                    }
                sqlite3_finalize(statement);
                sqlite3_close(database);
        }
        }
        
//        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
//        {
//        sqlite3_stmt* insertStatement;
//        
//        const char* sql = "insert into Events(ID,name,ticket,relevantDate,checked) values (?,?,?,?,?)";
//        
//        
//        if (sqlite3_prepare_v2(database, sql, -1, &insertStatement, NULL) != SQLITE_OK)
//        {
//            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
//        }
//        
//        
//        sqlite3_bind_text(insertStatement, 0, [[[fetchedObjects valueForKey:@"_id"] objectAtIndex:i] UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(insertStatement, 1, [[[[fetchedObjects valueForKey:@"userInfo"] valueForKey:@"firstName"] objectAtIndex:i] UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(insertStatement, 2, [[[fetchedObjects valueForKey:@"type"] objectAtIndex:i] UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(insertStatement, 3, [[[fetchedObjects valueForKey:@"updateDt"] objectAtIndex:i] UTF8String], -1, SQLITE_TRANSIENT);
//        
//        
//        int success = sqlite3_step(insertStatement);
//        sqlite3_finalize(insertStatement);
//        
//        if (success == SQLITE_ERROR)
//        {
//            NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
//        }
//        else
//        {
//            printf("\n Insertion Successful");
//        }
   // }
    }
}
-(void)Update:(NSString *)eventId
{
    if(sqlite3_open([databasePath UTF8String],&database )==SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        NSString *query = [NSString stringWithFormat:@"UPDATE EVENTS SET checked=1 where ID=%@",eventId];
        
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, NULL)==SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Updated Done");
                [self getTimings];
            }
            sqlite3_finalize(statement);
        }
    }
}
-(void)getTimings
{
    if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        //ID,name,ticket,relevantDate,checked
        NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM EVENTS"];
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSLog(@"ID: %s name: %s ticket:%s relevantDate:%s checked:%@", sqlite3_column_text(statement, 0), sqlite3_column_text(statement, 1),sqlite3_column_text(statement, 2), sqlite3_column_text(statement, 3),[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)]);
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
}
- (NSArray*)findByRegisterNumber
{
    const char *dbpath = [databasePath UTF8String];
    int count=0;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"select * from EVENTS"];
        sqlite3_stmt *statement;
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                ////ID,name,ticket,relevantDate,checked
                NSString *ID = [[NSString alloc] initWithUTF8String:
                                (const char *) sqlite3_column_text(statement, 0)];
                // [resultArray addObject:name];
                NSString *name = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 1)];
                // [resultArray addObject:department];
                NSString *ticket = [[NSString alloc]initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 2)];
                NSString *relevantDate = [[NSString alloc]initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 3)];
                NSString *checked = [[NSString alloc]initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 4)];
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: ID, @"ID", name, @"name", ticket, @"ticket",relevantDate, @"relevantDate", checked, @"checked", nil];
                [resultArray addObject:dict];
                //                NSLog(@"server checked%@--Local Checked%@",[[fetchedObjects valueForKey:@"checked"]objectAtIndex:count],checked);
                //                if([[[fetchedObjects valueForKey:@"checked"]objectAtIndex:count]integerValue])
                //                {
                //                    if([checked isEqualToString:@"1"])
                //                        NSLog(@"same in both");
                //                    else
                //                        NSLog(@"update to local");
                //                }
                //                else
                //                {
                //                    if([checked isEqualToString:@"1"])
                //                        NSLog(@"update to server");
                //                    else
                //                        NSLog(@"same in both");
                //                }
            }
            return resultArray;
        }
        sqlite3_reset(statement);
    }
    return nil;
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
