//
//  ManageEventsTableViewCell.m
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "ManageEventsTableViewCell.h"

@implementation ManageEventsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.timefontAwesome.font = [UIFont fontWithName:@"fontawesome" size:20.0f];
    self.timefontAwesome.text =@"\uf073";
    self.locationfontAwesome.font = [UIFont fontWithName:@"fontawesome" size:20.0f];
    self.locationfontAwesome.text =@"\uf041";
    // Configure the view for the selected state
}

@end
