//
//  CommonUtil.h
//  ANVI
//
//  Created by Hari Suresh Dosapati on 1/21/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>
#import <CoreLocation/CoreLocation.h>
#import "Services.h"
@interface CommonUtil : NSObject

+(UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;

+(NSUserDefaults *)getUserPreferences;
+(void) updateUserLocation:(CLLocation *)userLocation;
+ (NSMutableURLRequest *)requestPost:(NSString *)url input:(NSString *)jsonString;
+ (NSMutableURLRequest *)requestGet:(NSString *)url;
@end
