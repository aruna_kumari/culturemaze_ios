//
//  SignUpViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/18/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

#import "SWRevealViewController.h"
@interface SignUpViewController : UIViewController<UITextFieldDelegate,NSURLSessionDelegate>
{
        SWRevealViewController *revealController;
        NSString* email ;
        NSString* password ;
        NSString* confirmpassword ;
        NSString* description_ ;
        
        UITextField* emailField_ ;
        UITextField* passwordField_ ;
        UITextField* confirmpasswordField_ ;
    IBOutlet UITableView *signupTableView;
    NSArray* _permissions;

}
    // Creates a textfield with the specified text and placeholder text
-(UITextField*) makeTextField: (NSString*)text
placeholder: (NSString*)placeholder;
    // Handles UIControlEventEditingDidEndOnExit
- (IBAction)textFieldFinished:(id)sender ;
@property (nonatomic,copy) NSString* email ;
@property (nonatomic,copy) NSString* password ;
@property (nonatomic,copy) NSString* confirmpassword ;

@end