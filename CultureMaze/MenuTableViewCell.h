//
//  MenuTableViewCell.h
//  ANVI
//
//  Created by dotndot on 1/20/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
{
    IBOutlet UIButton *menuButton;
    
}
@property(nonatomic,strong)IBOutlet UIImageView *menuImage;
@end
