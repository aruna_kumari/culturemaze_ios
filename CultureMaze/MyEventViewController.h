//
//  MyEventViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyEventViewController : UIViewController<NSURLSessionDelegate>
{
    NSMutableArray *myEventData;
    IBOutlet UITableView *myEventTableView;
    IBOutlet UIButton *liveButton,*pastButton;

}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
