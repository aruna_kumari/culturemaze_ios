//
//  MenuTableViewController.m
//  ANVI
//
//  Created by dotndot on 1/20/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import "MenuTableViewController.h"
#import "MenuTableViewCell.h"
#import "SignUpViewController.h"
#import "UIImageView+WebCache.h"
@interface MenuTableViewController ()

@end

@implementation MenuTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    if([pref valueForKey:@"facebookid"])
    {
        logoutImage.backgroundColor=[UIColor colorWithRed:42/255.0 green:42/255.0 blue:42/255.0 alpha:1.0];
        [logoutImage.layer setCornerRadius:logoutImage.frame.size.width/2];
        logoutImage.layer.masksToBounds=YES;
        logoutImage.userInteractionEnabled=YES;
        UILabel *signin=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 70)];
        signin.font = [UIFont fontWithName:@"fontawesome" size:30.0f];
        signin.text =@"\uf011";
        signin.textColor=[UIColor whiteColor];
        signin.textAlignment=NSTextAlignmentCenter;
        [logoutImage addSubview:signin];
        UITapGestureRecognizer *logoutTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(logout)];
        logoutTap.numberOfTapsRequired=1;
        [logoutImage addGestureRecognizer:logoutTap];
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section
    if(section==0)
    {
        return 0;
    }
    else
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Menu";
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    UILabel *signin=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 70)];
    signin.font = [UIFont fontWithName:@"fontawesome" size:30.0f];
    if(indexPath.row==0)
    {
    signin.text =@"\uf0e7";
    }
    else if(indexPath.row==1)
    {
      signin.text =@"\uf044";
    }
    else if(indexPath.row==2)
    {
        signin.text =@"\uf0e5";
        
        //fa-power-off [&#xf011;
    }
    else if(indexPath.row==3)
    {
        signin.text =@"\uf013";
    }
    signin.textColor=[UIColor whiteColor];
    signin.textAlignment=NSTextAlignmentCenter;
    [cell.menuImage addSubview:signin];
    cell.menuImage.backgroundColor=[UIColor colorWithRed:42/255.0 green:42/255.0 blue:42/255.0 alpha:1.0];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        [self performSegueWithIdentifier:@"Latest" sender:nil];
    }
    if(indexPath.row==1)
    {
        [self performSegueWithIdentifier:@"Checkin" sender:nil];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 75, 75)];
    if(section==0)
    {
    UIImageView *profilePic=[[UIImageView alloc]initWithFrame:CGRectMake(15, 0, 70, 70)];
    profilePic.contentMode = UIViewContentModeScaleToFill;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    if([pref valueForKey:@"facebookid"])
    {
    [profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[pref valueForKey:@"facebookid"]]]];
    logoutImage.alpha=1.00;
    }
    else
    {
    logoutImage.backgroundColor=[UIColor clearColor];
    UILabel *signin=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 70)];
    signin.font = [UIFont fontWithName:@"fontawesome" size:30.0f];
    signin.text =@"\uf090";
    signin.textColor=[UIColor whiteColor];
    signin.textAlignment=NSTextAlignmentCenter;
    [profilePic addSubview:signin];
    profilePic.userInteractionEnabled=YES;
    UITapGestureRecognizer *loginTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(login)];
    loginTap.numberOfTapsRequired=1;
    [profilePic addGestureRecognizer:loginTap];
    }
    profilePic.backgroundColor=[UIColor colorWithRed:42/255.0 green:42/255.0 blue:42/255.0 alpha:1.0];
    [profilePic.layer setCornerRadius:profilePic.frame.size.width/2];
    profilePic.layer.masksToBounds=YES;
    [view addSubview:profilePic];
    }
    return view;
}
-(void)login
{
    [self performSegueWithIdentifier:@"Login" sender:nil];
}
-(IBAction)logout
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:nil forKey:@"facebookid"];
    [pref synchronize];
    logoutImage.alpha=0.00;
    [self.tableView reloadData];
}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 75, 75)];
//    if(section==1)
//    {
//        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
//        if([pref valueForKey:@"facebookid"])
//        {
//        UIImageView *profilePic=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 70, 70)];
//        profilePic.contentMode = UIViewContentModeScaleToFill;
//        profilePic.backgroundColor=[UIColor colorWithRed:42/255.0 green:42/255.0 blue:42/255.0 alpha:1.0];
//        [profilePic.layer setCornerRadius:profilePic.frame.size.width/2];
//        profilePic.layer.masksToBounds=YES;
//        profilePic.userInteractionEnabled=YES;
//        UILabel *signin=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 70, 70)];
//        signin.font = [UIFont fontWithName:@"fontawesome" size:30.0f];
//        signin.text =@"\uf011";
//        signin.textColor=[UIColor whiteColor];
//        signin.textAlignment=NSTextAlignmentCenter;
//        [profilePic addSubview:signin];
//        [view addSubview:profilePic];
//        UITapGestureRecognizer *logoutTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(logout)];
//        logoutTap.numberOfTapsRequired=1;
//        [profilePic addGestureRecognizer:logoutTap];
//        }
//    }
//    return view;
//}
//- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
//{
//    if ([segue.identifier isEqualToString:@"Signup"])
//    {
//        SWRevealViewControllerSegue* rvcs = (SWRevealViewControllerSegue*) segue;
//        SWRevealViewController* rvc = self.revealViewController;
//        NSAssert( rvc != nil, @"oops! must have a revealViewController" );
//        NSAssert( [rvc.frontViewController isKindOfClass:[UINavigationController class]], @"oops!  for this segue we want a permanent navigation controller in the front!" );
//        rvcs.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
//        {
//            UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:dvc];
//            nc.navigationBar.translucent=NO;
//            [rvc setFrontViewController:nc animated:YES];
//        };
//    }
//    else if([segue isKindOfClass: [SWRevealViewControllerSegue class]] )
//    {
//        SWRevealViewControllerSegue* rvcs = (SWRevealViewControllerSegue*) segue;
//        SWRevealViewController* rvc = self.revealViewController;
//        NSAssert( rvc != nil, @"oops! must have a revealViewController" );
//        NSAssert( [rvc.frontViewController isKindOfClass: [UINavigationController class]], @"oops!  for this segue we want a permanent navigation controller in the front!" );
//        rvcs.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
//        {
//            UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:dvc];
//            nc.navigationBar.translucent=NO;
//            [rvc setFrontViewController:nc animated:YES];
//        };
//        
//    }
//}
//-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
//{
//    if(section == 1)
//        return 95;
//    return 1.0;
//}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 95;
    return 1.0;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
