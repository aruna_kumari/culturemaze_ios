//
//  ManageEventViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "ManageEventViewController.h"
#import "ManageEventsTableViewCell.h"
@interface ManageEventViewController ()

@end

@implementation ManageEventViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    listofCategories=[[NSMutableArray alloc]initWithObjects:@"",@"Sales & Analytics",@"edit tickets",@"manage orders",@"manage bundles",@"manage attendees",@"invite & promote",@"track visit",@"track bundle requests", nil];
    [manageEventTableView reloadData];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [listofCategories count];
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [listofCategories objectAtIndex:section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section==0)
    {
        return 1;
    }
    else{
        return 4;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0)
    {
        return 173;
    }
    else
    {
        return 57;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 10;
    }
    return 30;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ManageEventsTableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        if(indexPath.section==0)
        [tableView registerNib:[UINib nibWithNibName:@"ManageEventsTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        else
        [tableView registerNib:[UINib nibWithNibName:@"ManageEventCategory" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];;
    }
    if(indexPath.section==0)
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSMutableArray *array=[pref objectForKey:@"ManageEvent"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // here set format of date which is in your output date (means above str with format)
        NSDate *date = [dateFormatter dateFromString: [[array valueForKey:@"start"]valueForKey:@"date"]]; // here you can fetch date from string with define format
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy | HH:ss a"]; // here set format which you want...
        NSString *convertedString = [dateFormatter stringFromDate:date];
        cell.time.text=convertedString;
        cell.title.text=[NSString stringWithFormat:@"%@",[[array valueForKey:@"title"]valueForKey:@"text"]];
        [cell.category setTitle:[NSString stringWithFormat:@"#%@",[[array valueForKey:@"category"]objectAtIndex:0]] forState:UIControlStateNormal];
        
        [cell.culture setTitle:[NSString stringWithFormat:@"#%@",[[array valueForKey:@"culture"]objectAtIndex:0]] forState:UIControlStateNormal];
        
        NSString *address=nil;
        if([[array valueForKey:@"venue"]valueForKey:@"address"])
        {
            address=[[array valueForKey:@"venue"]valueForKey:@"address"];
        }
        else{
            address=@"";
        }
        cell.location.text=[NSString stringWithFormat:@"%@",address];
        cell.time.alpha=1.00;
        cell.culture.alpha=1.00;
        cell.category.alpha=1.00;
        cell.timefontAwesome.alpha=1.00;
        cell.locationfontAwesome.alpha=1.00;
        cell.location.alpha=1.00;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        cell.title.text=@"checkin";
        cell.time.alpha=0.00;
        cell.location.alpha=0.00;
        cell.timefontAwesome.alpha=0.00;
        cell.locationfontAwesome.alpha=0.00;
        cell.culture.alpha=0.00;
        cell.category.alpha=0.00;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
   
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section!=0)
    {
        [self performSegueWithIdentifier:@"Checkin" sender:nil];
    }
}

-(IBAction)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
