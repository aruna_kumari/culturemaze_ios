//
//  CultureCategoryViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/20/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CultureCategoryViewController : UIViewController<NSURLSessionDelegate>
{
    NSMutableArray *cultureData;
    NSMutableDictionary *cultureStatusData;
    IBOutlet UITableView *cultureTableView;
}
@end
