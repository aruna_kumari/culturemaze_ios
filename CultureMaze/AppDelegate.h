//
//  AppDelegate.h
//  CultureMaze
//
//  Created by dotndot on 6/17/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import <sqlite3.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    NSString        *databasePath;
    sqlite3 *database;
}
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)CLLocationManager *locationManager;
-(IBAction)getOfflineData:(NSArray *)fetchedObjects;
-(void)Update:(NSString *)eventId;
- (NSArray*)findByRegisterNumber;
-(void)getTimings;


@end

