//
//  SignUpViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/18/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "SignUpViewController.h"
#import "MenuTableViewCell.h"
#import "Services.h"
#import "SVProgressHUD.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize email = email_ ;
@synthesize confirmpassword = confirmpassword_;
@synthesize password = password_ ;
- (void)viewDidLoad
{
   [super viewDidLoad];
   self.email        = @"" ;
   self.confirmpassword     = @"" ;
   self.password    = @"" ;
    [signupTableView reloadData];
}
#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(section==0)
    return 3;
    else if(section==1)
    return 2;
    else
        return 2;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Menu";
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // Make cell unselectable
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.section==0)
    {
    UITextField* tf = nil ;
    switch ( indexPath.row )
    {
        case 0: {
            tf = emailField_ = [self makeTextField:self.email placeholder:@"Email"];
            [cell addSubview:emailField_];
            tf.secureTextEntry=NO;
            break ;
        }
        case 1: {
            tf = passwordField_ = [self makeTextField:self.password placeholder:@"Password"];
            tf.secureTextEntry=YES;
            [cell addSubview:passwordField_];
            break ;
        }
        case 2: {
            tf = confirmpasswordField_ = [self makeTextField:self.confirmpassword placeholder:@"Confirm Password"];
            tf.secureTextEntry=YES;
            [cell addSubview:confirmpasswordField_];
            break ;
        }
    }
    // Textfield dimensions
    tf.frame = CGRectMake(20, 15, self.view.frame.size.width-40, 30);
    tf.font=[UIFont systemFontOfSize:18];
    // Workaround to dismiss keyboard when Done/Return is tapped
    [tf addTarget:self action:@selector(textFieldFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];	
    // We want to handle textFieldDidEndEditing
    tf.delegate = self ;
      CALayer *border = [CALayer layer];
        CGFloat borderWidth = 1;
        border.borderColor = [UIColor lightGrayColor].CGColor;
        border.frame = CGRectMake(0, tf.frame.size.height - borderWidth, tf.frame.size.width, tf.frame.size.height);
        border.borderWidth = borderWidth;
        [tf.layer addSublayer:border];
        tf.layer.masksToBounds = YES;
    }
    else if(indexPath.section==2)
    {
        switch ( indexPath.row )
        {
            case 0:
            {
                UIButton *signUp=[[UIButton alloc]initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 60)];
                [signUp setTitle:@"Later" forState:UIControlStateNormal];
                signUp.backgroundColor=[UIColor clearColor];
                [signUp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                signUp.layer.cornerRadius=5;
                [signUp.titleLabel setTextAlignment:NSTextAlignmentCenter];
                [cell addSubview:signUp];
                break ;
            }
            case 1:
            {
                UIButton *signUp=[[UIButton alloc]initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 60)];
                [signUp setTitle:@"Already have an account" forState:UIControlStateNormal];
                [signUp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                signUp.backgroundColor=[UIColor clearColor];
                [signUp.titleLabel setTextAlignment:NSTextAlignmentCenter];
                signUp.layer.cornerRadius=5;
                [cell addSubview:signUp];
                break ;
            }
        }
    }
    else
    {
        switch ( indexPath.row )
        {
            case 0:
            {
                UIButton *signUp=[[UIButton alloc]initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 60)];
                [signUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
                [signUp addTarget:self action:@selector(signUp:) forControlEvents:UIControlEventTouchUpInside];
                signUp.backgroundColor=[UIColor colorWithRed:134/255.0 green:192/255.0 blue:94/255.0f alpha:1.0];
                signUp.layer.cornerRadius=5;
                [cell addSubview:signUp];
                break ;
            }
            case 1:
            {
                UIButton *signUp=[[UIButton alloc]initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 60)];
                [signUp setTitle:@"SIGN UP WITH FACEBOOK" forState:UIControlStateNormal];
                [signUp addTarget:self action:@selector(fblogin) forControlEvents:UIControlEventTouchUpInside];
                signUp.backgroundColor=[UIColor colorWithRed:75/255.0 green:98/255.0 blue:161/255.0f alpha:1.0];
                signUp.layer.cornerRadius=5;
                [cell addSubview:signUp];
                break ;
            }
        }
    }
    return cell;
}
-(void)signUp:(UIButton *)sender
{
    NSLog(@"%@",emailField_.text);
}
- (IBAction)fblogin
{
    //[FBSession.activeSession closeAndClearTokenInformation];
    [SVProgressHUD show];
     _permissions = [[NSArray alloc] initWithObjects:@"read_stream",@"offline_access",@"email",@"friends_location",@"status_update",nil];
    FBSession* sess = [[FBSession alloc] initWithPermissions:_permissions];
    [FBSession setActiveSession:sess];
    [sess openWithBehavior:(FBSessionLoginBehaviorWithFallbackToWebView) completionHandler:^(FBSession *session, FBSessionState status, NSError *error){
    [self sessionStateChanged:session state:status error:error];
    }];
}
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state)
    {
        case FBSessionStateOpen:
        {
            [self getUserDetails];
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // Once the user has logged in, we want them to
            // be looking at the root view.
            // [FBSession.activeSession closeAndClearTokenInformation];
            
            break;
        default:
            break;
    }
    if (error)
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Connection error", nil)
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
- (void)getUserDetails
{
    if (FBSession.activeSession.isOpen)
    {
        
        [FBRequestConnection startWithGraphPath:@"me"
                                     parameters:nil
                                     HTTPMethod:@"GET"
                              completionHandler:^(
                                                  FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error
                                                  )
        {
                                  NSLog(@"result %@",result);
                                  NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                  [pref setValue:[result valueForKey:@"id"] forKey:@"facebookid"];
                                  [pref synchronize];
                                  [self back];
                                  [self facebookLogin:result];
            
        }];
}
}
-(void)facebookLogin:(NSMutableDictionary *)data
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSError *error;
    [data setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *URLPath = [NSString stringWithFormat:@"%@/v1/moblie/addFacebookProvider",serverURL];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
   
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              if (res && [res isKindOfClass:[NSDictionary class]])
                                              {
//                                                  NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
//                                                  [pref setValue:[res valueForKey:@"id"] forKey:@"userid"];
//                                                  [pref synchronize];
                                                  [SVProgressHUD dismiss];
                                              }
                                          }
                                          else
                                          {
                                          }
                                      }];
    [dataTask resume];
}
-(UITextField*) makeTextField: (NSString*)text
                  placeholder: (NSString*)placeholder  {
    UITextField *tf = [[UITextField alloc] init];
    tf.placeholder = placeholder ;
    tf.text = text ;
    tf.autocorrectionType = UITextAutocorrectionTypeNo ;
    tf.autocapitalizationType = UITextAutocapitalizationTypeNone;
    tf.adjustsFontSizeToFitWidth = YES;
    tf.textColor = [UIColor colorWithRed:56.0f/255.0f green:84.0f/255.0f blue:135.0f/255.0f alpha:1.0f];
    return tf ;
}

// Workaround to hide keyboard when Done is tapped
- (IBAction)textFieldFinished:(id)sender {
    // [sender resignFirstResponder];
}

// Textfield value changed, store the new value.
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ( textField == emailField_ ) {
        self.email = textField.text ;
    } else if ( textField == confirmpasswordField_ ) {
        self.confirmpassword = textField.text ;
    } else if ( textField == passwordField_ ) {
        self.password = textField.text ;
    }
}

-(IBAction)back
{
    [self performSegueWithIdentifier:@"Home" sender:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
