//
//  MenuTableViewController.h
//  ANVI
//
//  Created by dotndot on 1/20/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface MenuTableViewController : UITableViewController
{
    IBOutlet UIImageView *logoutImage;
}
@end
