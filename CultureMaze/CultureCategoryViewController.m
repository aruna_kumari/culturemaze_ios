//
//  CultureCategoryViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/20/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "CultureCategoryViewController.h"
#import "HomeTableViewCell.h"
#import "Services.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
@interface CultureCategoryViewController ()

@end

@implementation CultureCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    cultureData=[[NSMutableArray alloc]init];
    cultureStatusData=[[NSMutableDictionary alloc]init];
    [SVProgressHUD show];
    [self getAccountInfoWithCompletion];
    // Do any additional setup after loading the view.
}

- (void)getAccountInfoWithCompletion
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    self.title=[user valueForKey:@"cName"];
    NSString *name=[[user valueForKey:@"cName"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString *urlString=[NSString stringWithFormat:@"%@/v1/events/search/%@/-/-/-/-/%@/%@/-/-/-/-/-/-/-/-",serverURL,[user valueForKey:@"userid"],[user valueForKey:@"actionType"],name];
   
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:urlString
                              ]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              NSMutableArray *array=[res valueForKey:@"data"];
                                              for(int i=0;i<[array count];i++)
                                              {
                                                  [cultureData addObject:[array objectAtIndex:i]];
                                              }
                                              [cultureTableView reloadData];
                                              [SVProgressHUD dismiss];
                                          }
                                          else
                                          {
                                              [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
                                          }
                                      }];
    [dataTask resume];
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    NSString *host = challenge.protectionSpace.host;
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        if ([host rangeOfString:@"qa.budagaa.com"].location != NSNotFound)
        {
            completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
        else
        {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge,nil);
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [cultureData  count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeTableViewCell *cell;
    //Get a cell instance (either dequeue from tableView or allocate a new one).
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];;
    }
    if([[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"bannerImgRefs"] count]==0)
    {
        if([[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"] count]!=0)
        {
        [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"]objectAtIndex:0]]]];
        }
    }
    else
    {
        [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"bannerImgRefs"]objectAtIndex:0]]]];
    }
    cell.saveButton.tag=indexPath.row;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // here set format of date which is in your output date (means above str with format)
    NSDate *date = [dateFormatter dateFromString: [[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"start"]valueForKey:@"date"]]; // here you can fetch date from string with define format
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy | HH:ss a"]; // here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date];
    cell.dateLabel.text=convertedString;
    cell.eventTitle.text=[NSString stringWithFormat:@"%@",[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"title"]valueForKey:@"text"]];
    [cell.categoryLabel setTitle:[NSString stringWithFormat:@"#%@",[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"category"]objectAtIndex:0]] forState:UIControlStateNormal];
    
    [cell.cultureLabel setTitle:[NSString stringWithFormat:@"#%@",[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"culture"]objectAtIndex:0]] forState:UIControlStateNormal];
    
    NSString *address=nil;
    if([[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"])
    {
        address=[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"];
    }
    else
    {
        address=@"";
    }
    cell.cityLabel.text=address;
    cell.likeCount.text=[NSString stringWithFormat:@"%d",[[[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"counts"]valueForKey:@"views"]intValue]];
    cell.saveLabel.font = [UIFont fontWithName:@"fontawesome" size:28.0f];
    
    [cell.cultureLabel addTarget:self action:@selector(cultureData:) forControlEvents:UIControlEventTouchUpInside];
    cell.cultureLabel.tag=indexPath.row;
    cell.categoryLabel.tag=indexPath.row;
    [cell.categoryLabel addTarget:self action:@selector(categoryData:) forControlEvents:UIControlEventTouchUpInside];
    if([cultureStatusData valueForKey:[NSString stringWithFormat:@"%ld",(long)cell.saveButton.tag]])
    {
        cell.saveLabel.text=@"\uf004";
        cell.saveLabel.textColor=[UIColor blueColor];
        [cell.saveButton addTarget:self action:@selector(deleteEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.saveLabel.text =@"\uf08a";
        cell.saveLabel.textColor=[UIColor whiteColor];
        [cell.saveButton addTarget:self action:@selector(saveEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    if(!cell.saveORdelete)
    {
    NSMutableArray *array=[[cultureData  objectAtIndex:indexPath.row]valueForKey:@"actionTypes"];
    if([array containsObject:@"save"])
    {
        cell.saveLabel.text=@"\uf004";
        cell.saveLabel.textColor=[UIColor blueColor];
        [cell.saveButton addTarget:self action:@selector(deleteEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.saveButton addTarget:self action:@selector(saveEvent:) forControlEvents:UIControlEventTouchUpInside];

    }
    }

    return cell;
}
-(void)saveEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[cultureTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf004";
    cell.saveLabel.textColor=[UIColor blueColor];
    cell.saveORdelete=TRUE;
    [cultureStatusData setValue:[NSString stringWithFormat:@"%ld", (long)sender.tag] forKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[cultureData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"A" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *URLPath = [NSString stringWithFormat:@"%@/v1/events/updateCounts",serverURL];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        if (!error && responseCode == 200)
        {
            id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if(res && [res isKindOfClass:[NSDictionary class]])
            {
            }
        }
        else
        {
            if([[error domain] isEqualToString:@"NSURLErrorDomain"])
            {
            }
        }
        [cultureTableView reloadData];
    }];
}
-(void)deleteEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[cultureTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf08a";
    cell.saveLabel.textColor=[UIColor whiteColor];
    cell.saveORdelete=TRUE;
    [cultureStatusData  removeObjectForKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[cultureData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"D" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *URLPath = [NSString stringWithFormat:@"%@/v1/events/updateCounts",serverURL];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        if (!error && responseCode == 200)
        {
            id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (res && [res isKindOfClass:[NSDictionary class]])
            {
                
            }
        }
        else
        {
            if([[error domain] isEqualToString:@"NSURLErrorDomain"])
            {
                
            }
        }
        
    }];
}
-(void)cultureData:(UIButton *)sender
{
    [SVProgressHUD show];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[cultureData  objectAtIndex:sender.tag]valueForKey:@"culture"]objectAtIndex:0]] forKey:@"cName"];
    [pref setValue:@"culture" forKey:@"actionType"];
    [pref synchronize];
    cultureData=[[NSMutableArray alloc]init];
    [self getAccountInfoWithCompletion];
    [cultureTableView reloadData];
    
}
-(void)categoryData:(UIButton *)sender
{
    [SVProgressHUD show];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[cultureData  objectAtIndex:sender.tag]valueForKey:@"category"]objectAtIndex:0]]  forKey:@"cName"];
    [pref setValue:@"category" forKey:@"actionType"];
    [pref synchronize];
    cultureData=[[NSMutableArray alloc]init];
    [self getAccountInfoWithCompletion];
    [cultureTableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
