//
//  CommonUtil.m
//  ANVI
//
//  Created by Hari Suresh Dosapati on 1/21/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import "CommonUtil.h"
#import "Services.h"

@implementation CommonUtil


+(UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexInt = 0;
    // Create scanner` 2
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    //[self intFromHexString:hexStr];
    unsigned int hexint =hexInt;
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    return color;
}
+(NSUserDefaults *)getUserPreferences
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    return prefs;
}
+(void) updateUserLocation:(CLLocation *)userLocation
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    int degrees = userLocation.coordinate.latitude;
    double decimal = fabs(userLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    // NSString *lat = [NSString stringWithFormat:@"%d° %d' %1.4f\"",degrees, minutes, seconds];
    NSString *lat = [NSString stringWithFormat:@"%d° %d' %1.4f\"",degrees, minutes, seconds];
    degrees = userLocation.coordinate.longitude;
    decimal = fabs(userLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    NSString *longt = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                       degrees, minutes, seconds];
    //save latitude and longitude values are locally
    [prefs setValue:[NSString stringWithFormat:@"%f",userLocation.coordinate.latitude] forKey:@"lat"];
    [prefs setValue:[NSString stringWithFormat:@"%f",userLocation.coordinate.longitude] forKey:@"long"];
    [prefs synchronize];
     prefs=nil;
     lat=nil;
     longt=nil;
}
+ (NSMutableURLRequest *)requestPost:(NSString *)url input:(NSString *)jsonString
{
    NSString *string=url;
    NSURL *URL = [NSURL URLWithString:[string stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    return request;
}
+ (NSMutableURLRequest *)requestGet:(NSString *)url
{
    NSString *string=url;
    NSURL *URL = [NSURL URLWithString:[string stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"GET"];
    return request;
}

@end
