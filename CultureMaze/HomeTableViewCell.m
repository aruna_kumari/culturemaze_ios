//
//  HomeTableViewCell.m
//  ANVI
//
//  Created by dotndot on 1/20/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "UIImageView+WebCache.h"
@implementation HomeTableViewCell
@synthesize detailEventButton;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    eventView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    eventView.layer.borderWidth=.5;
    eventView.layer.cornerRadius = 3;
    eventView.layer.shadowColor = [UIColor grayColor].CGColor;
    eventView.layer.shadowOpacity = 0.3f;
    eventView.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    eventView.layer.shadowRadius = 5.0f;
    eventView.layer.masksToBounds = NO;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:eventView.bounds];
    eventView.layer.shadowPath=path.CGPath;
    self.profilePic.layer.cornerRadius =self.profilePic.frame.size.width/2;
    self.profilePic.layer.masksToBounds = YES;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    if([pref valueForKey:@"facebookid"])
    {
        [self.profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[pref valueForKey:@"facebookid"]]]];
    }
//    self.saveLabel.font = [UIFont fontWithName:@"fontawesome" size:28.0f];
//    self.saveLabel.text =@"\uf08a";
    self.loationFontasesome.font = [UIFont fontWithName:@"fontawesome" size:20.0f];
    self.loationFontasesome.text =@"\uf041";
    self.timeFontawesome.font = [UIFont fontWithName:@"fontawesome" size:20.0f];
    self.timeFontawesome.text =@"\uf017";
}

@end
