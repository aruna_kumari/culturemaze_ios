//
//  EventTicket.h
//  CultureMaze
//
//  Created by dotndot on 6/26/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventTicket : NSObject
@property (nonatomic, retain) NSString* name;

-(id)initWithName:(NSString *)name;

@end
