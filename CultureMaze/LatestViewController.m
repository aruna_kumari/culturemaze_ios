//
//  LatestViewController.m
//  CultureMaze
//
//  Created by dotndot on 6/22/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "LatestViewController.h"
#import "SVProgressHUD.h"
#import "SVPullToRefresh.h"
#import "Services.h"
#import "HomeTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface LatestViewController ()

@end

@implementation LatestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *centerNavigationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [centerNavigationButton setTitle:@"CultureMaze" forState:UIControlStateNormal];
    centerNavigationButton.frame = (CGRect)
    {
        .size.width = 100,
        .size.height = 20,
    };
    [centerNavigationButton addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setTitleView:centerNavigationButton];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    // Do any additional setup after loading the view.
    __weak LatestViewController *weakSelf = self;
    [latestEventsTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertBottom];
    }];
    latestEventData=[[NSMutableArray alloc]init];
    latestStatusData=[[NSMutableDictionary alloc]init];
    page=1;
    [self getLatestEventsData];
    [SVProgressHUD show];
}
-(void)home
{
    [self performSegueWithIdentifier:@"Home" sender:nil];
}
-(void)insertBottom
{
    page++;
    int64_t delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [latestEventsTableView  beginUpdates];
        [self getLatestEventsData];
        [latestEventsTableView endUpdates];
        [latestEventsTableView.infiniteScrollingView stopAnimating];
    });
}
- (void)getLatestEventsData
{
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
   
    
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:
                              [NSString stringWithFormat:@"%@/v1/events/search/%@/-/-/-/-/-/-/-/%d/-/-/_id/-/-/-",serverURL,[user valueForKey:@"userid"],page]
                              ]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    defaultConfigObject.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                          if (error == nil && data != nil)
                                          {
                                              id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                              NSMutableArray *array=[res valueForKey:@"data"];
                                              for(int i=0;i<[array count];i++)
                                              {
                                                  [latestEventData addObject:[array objectAtIndex:i]];
                                              }
                                              [SVProgressHUD dismiss];
                                              [latestEventsTableView reloadData];
                                              if([latestEventData count]==0)
                                              {
                                                  [SVProgressHUD showErrorWithStatus:@"No Events are Available"];
                                              }
                                          }
                                          else
                                          {
                                              [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
                                          }
                                      }];
    [dataTask resume];
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    NSString *host = challenge.protectionSpace.host;
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        if ([host rangeOfString:@"qa.budagaa.com"].location != NSNotFound)
        {
            completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
        else
        {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge,nil);
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
   
        return 1;
   
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
  
        return [latestEventData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeTableViewCell *cell;
    //Get a cell instance (either dequeue from tableView or allocate a new one).
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];;
    }
    if([[[latestEventData  objectAtIndex:indexPath.row] valueForKey:@"imgRef"]  isEqual:[NSNull null]] &&  [[[latestEventData  objectAtIndex:indexPath.row] valueForKey:@"imgRef"]  length] == 0 )
    {
           if([[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"] count]!=0)
            {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"imgRefs"]objectAtIndex:0]]]];
            }
    }
        else
        {
            [cell.eventImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"imgRef"]]]];
        }
        cell.saveButton.tag=indexPath.row;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]; // here set format of date which is in your output date (means above str with format)
        NSDate *date = [dateFormatter dateFromString: [[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"start"]valueForKey:@"date"]]; // here you can fetch date from string with define format
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy | HH:ss a"]; // here set format which you want...
        NSString *convertedString = [dateFormatter stringFromDate:date];
        cell.dateLabel.text=convertedString;
        cell.eventTitle.text=[NSString stringWithFormat:@"%@",[[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"title"]valueForKey:@"text"]];
        [cell.categoryLabel setTitle:[NSString stringWithFormat:@"#%@",[[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"category"]objectAtIndex:0]] forState:UIControlStateNormal];
        
        [cell.cultureLabel setTitle:[NSString stringWithFormat:@"#%@",[[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"culture"]objectAtIndex:0]] forState:UIControlStateNormal];
        
    NSString *address=nil;
    if([[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"])
    {
        address=[[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"venue"]valueForKey:@"address"];
    }
    else{
        address=@"";
    }
        cell.cityLabel.text=[NSString stringWithFormat:@"%@",address];
        cell.likeCount.text=[NSString stringWithFormat:@"%d",[[[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"counts"]valueForKey:@"views"]intValue]];
        cell.saveLabel.font = [UIFont fontWithName:@"fontawesome" size:28.0f];
        [cell.cultureLabel addTarget:self action:@selector(cultureData:) forControlEvents:UIControlEventTouchUpInside];
        cell.cultureLabel.tag=indexPath.row;
        cell.categoryLabel.tag=indexPath.row;
        [cell.categoryLabel addTarget:self action:@selector(categoryData:) forControlEvents:UIControlEventTouchUpInside];
    if([latestStatusData valueForKey:[NSString stringWithFormat:@"%ld",(long)cell.saveButton.tag]])
    {
    cell.saveLabel.text=@"\uf004";
    cell.saveLabel.textColor=[UIColor blueColor];
    [cell.saveButton addTarget:self action:@selector(deleteEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
    cell.saveLabel.text =@"\uf08a";
    cell.saveLabel.textColor=[UIColor whiteColor];
    [cell.saveButton addTarget:self action:@selector(saveEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    if(!cell.saveORdelete)
    {
    NSMutableArray *array=[[latestEventData  objectAtIndex:indexPath.row]valueForKey:@"actionTypes"];
    if([array containsObject:@"save"])
    {
    cell.saveLabel.text=@"\uf004";
    cell.saveLabel.textColor=[UIColor blueColor];
    [cell.saveButton addTarget:self action:@selector(deleteEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
    [cell.saveButton addTarget:self action:@selector(saveEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    }
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor=[UIColor colorWithRed:241.0/255.0f green:245/255.0f blue:247/255.0 alpha:1.0];
    UILabel *headerTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, self.view.frame.size.width, 42)];
    headerTitle.backgroundColor=[UIColor whiteColor];
    headerTitle.font=[UIFont systemFontOfSize:14.00];
    headerTitle.text=@"  LATEST EVENTS";
    headerTitle.textColor=[UIColor darkGrayColor];
    [view addSubview:headerTitle];
    return view;
}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 52;
}
-(void)cultureData:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[latestEventData  objectAtIndex:sender.tag]valueForKey:@"culture"]objectAtIndex:0]] forKey:@"cName"];
    [pref setValue:@"culture" forKey:@"actionType"];
    [pref synchronize];
    [self performSegueWithIdentifier:@"Culture" sender:nil];
}
-(void)categoryData:(UIButton *)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setValue:[NSString stringWithFormat:@"%@",[[[latestEventData  objectAtIndex:sender.tag]valueForKey:@"category"]objectAtIndex:0]]  forKey:@"cName"];
    [pref setValue:@"category" forKey:@"actionType"];
    [pref synchronize];
    [self performSegueWithIdentifier:@"Culture" sender:nil];
}
-(void)updateCounts:(NSString *)jsonString
{
    NSString *URLPath = [NSString stringWithFormat:@"%@/v1/events/updateCounts",serverURL];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        if (!error && responseCode == 200)
        {
            id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (res && [res isKindOfClass:[NSDictionary class]])
            {
                
            }
        }
        else
        {
            if([[error domain] isEqualToString:@"NSURLErrorDomain"])
            {
                
            }
        }
        [latestEventsTableView reloadData];
        
    }];
}
-(void)saveEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[latestEventsTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf004";
    cell.saveLabel.textColor=[UIColor blueColor];
    cell.saveORdelete=YES;
    [latestStatusData setValue:[NSString stringWithFormat:@"%ld", (long)sender.tag] forKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[latestEventData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"A" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self updateCounts:jsonString];
}
-(void)deleteEvent:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    HomeTableViewCell *cell =(HomeTableViewCell *)[latestEventsTableView cellForRowAtIndexPath:indexPath];
    cell.saveLabel.text=@"\uf08a";
    cell.saveLabel.textColor=[UIColor whiteColor];
    cell.saveORdelete=YES;
    
    [latestStatusData removeObjectForKey:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *passingArray = [[NSMutableDictionary alloc] init];
    [passingArray setObject:[[latestEventData  objectAtIndex:sender.tag]valueForKey:@"_id"] forKey:@"_id"];
    [passingArray setObject:[user valueForKey:@"userid"] forKey:@"userId"];
    [passingArray setObject:@"save" forKey:@"actionType"];
    [passingArray setObject:@"D" forKey:@"status"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passingArray
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self updateCounts:jsonString];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
