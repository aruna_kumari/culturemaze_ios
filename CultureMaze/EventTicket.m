//
//  EventTicket.m
//  CultureMaze
//
//  Created by dotndot on 6/26/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "EventTicket.h"

@implementation EventTicket
-(id)initWithName:(NSString *)name
{
    self = [super init];
    if(self)
    {
        self.name = name;
    }
    return self;
}
@end
