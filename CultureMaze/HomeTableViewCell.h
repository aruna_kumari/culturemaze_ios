//
//  HomeTableViewCell.h
//  ANVI
//
//  Created by dotndot on 1/20/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell
{
    IBOutlet UIView *eventView;
}
@property(nonatomic,strong)    IBOutlet UIButton *detailEventButton,*saveButton,*cultureLabel,*categoryLabel;
@property(nonatomic,strong)    IBOutlet UIImageView *eventImage,*profilePic;
@property(nonatomic,strong)    IBOutlet UILabel *eventTitle,*dateLabel,*username,*likeCount,*saveLabel,*cityLabel,*timeFontawesome,*loationFontasesome;
@property(nonatomic)BOOL saveORdelete;
@end
