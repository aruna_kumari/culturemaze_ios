//
//  MenuTableViewCell.m
//  ANVI
//
//  Created by dotndot on 1/20/15.
//  Copyright (c) 2015 ANVI. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell
@synthesize menuImage;
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    [menuImage.layer setCornerRadius:self.menuImage.frame.size.width/2];
    // Configure the view for the selected state
}

@end
