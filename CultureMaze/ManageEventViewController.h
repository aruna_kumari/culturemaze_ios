//
//  ManageEventViewController.h
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageEventViewController : UIViewController
{
    NSMutableArray *listofCategories;
    IBOutlet UITableView *manageEventTableView;
}
@end
