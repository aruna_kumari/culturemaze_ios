//
//  MyEventTableViewCell.m
//  CultureMaze
//
//  Created by dotndot on 6/30/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "MyEventTableViewCell.h"

@implementation MyEventTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    eventView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    eventView.layer.borderWidth=.5;
    eventView.layer.cornerRadius = 3;
    eventView.layer.shadowColor = [UIColor grayColor].CGColor;
    eventView.layer.shadowOpacity = 0.3f;
    eventView.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    eventView.layer.shadowRadius = 5.0f;
    eventView.layer.masksToBounds = NO;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:eventView.bounds];
    eventView.layer.shadowPath=path.CGPath;
   
    self.loationFontasesome.font = [UIFont fontWithName:@"fontawesome" size:20.0f];
    self.loationFontasesome.text =@"\uf041";
    self.timeFontawesome.font = [UIFont fontWithName:@"fontawesome" size:20.0f];
    self.timeFontawesome.text =@"\uf073";
    // Configure the view for the selected state
}

@end
