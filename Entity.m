//
//  Entity.m
//  CultureMaze
//
//  Created by dotndot on 6/25/15.
//  Copyright (c) 2015 SMD. All rights reserved.
//

#import "Entity.h"


@implementation Entity

@dynamic checked;
@dynamic ticketid;
@dynamic name;
@dynamic relevantDate;
@dynamic ticket;

@end
